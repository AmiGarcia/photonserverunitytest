﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{

    public static UIManager uiInstance;
    public GameObject hudGame, menu;
    public bool created = false;


    //void Awake()
    //{
    //    if (!created)
    //    {
    //        //do this for the first time to ensure persistance
    //        DontDestroyOnLoad(this.gameObject);
    //        uiInstance = this;
    //        created = true;
    //    }
    //    else
    //    {
    //        //must be a duplicated object, so DESTROY!
    //        Debug.Log("!!!!!! DUPLICADOOOOO");
    //        Destroy(this.gameObject);
    //    }
    //}
    private void Start()
    {
        if (!created)
        {
            //do this for the first time to ensure persistance
            DontDestroyOnLoad(this.gameObject);
            uiInstance = this;
            created = true;
        }
        else
        {
            //must be a duplicated object, so DESTROY!
            Debug.Log("!!!!!! DUPLICADOOOOO");
            Destroy(this.gameObject);
        }
        
    }
    public void hudGameManager(bool isActive){
        hudGame.SetActive(isActive);
    }
    public void menuManager(bool isActive){
        menu.SetActive(isActive);
    }
    public void exitMap(){
        Debug.Log("EXIT FROM MAP!!");
        NetworkingManager.instance.DisconectFromCurrentRoom();
        SceneManager.LoadScene("SceneManager", LoadSceneMode.Single);
    }
    public void gotoSchool(){
        NetworkingManager.instance.ConnectToRoom(NetworkingManager.roomType.School);
    }
    public void gotoPlayground(){
        NetworkingManager.instance.ConnectToRoom(NetworkingManager.roomType.Playground);
    }

}
