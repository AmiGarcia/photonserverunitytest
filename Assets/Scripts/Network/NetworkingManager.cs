﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
      THIS IS THE CLASS FOR MANAGE THE NETWORK (AND BELONGS TO SCENE MANAGER)
      first of all, you have to follow those structions:
      1 - Connect to master Lobby (PUN SERVER)
      2 - Try to connect an existent room for the map you want
      3 - If there isnt a room avaiable to connect, then create one
      4 - When joining a room, check if u r the master client and the first to enter. If yes, load the right level. If not, just play arround.
 */

public class NetworkingManager : Photon.PunBehaviour
{

    static public NetworkingManager instance; 
    public Vector3 spawnPointPosition = new Vector3(-2.55f,-1.63f,0f);
    public GameObject spawn;
    private static bool created = false;
    public enum roomType { Playground, School };
    ExitGames.Client.Photon.Hashtable expectedCustomPropertiesSchool, expectedCustomPropertiesPlayground;
    RoomOptions roomOptions;

    private string version = "v0.1";
    private bool isConnected = false;
    private roomType map;

    void Awake()
    {
        if(!created){
            //do this for the first time to ensure persistance
            DontDestroyOnLoad(this.gameObject);
            instance = this;
            created = true;
        }else{
            //must be a duplicated object, so DESTROY!
            Destroy(this.gameObject);
        }

        // #Critical
        // we don't join the lobby. There is no need to join a lobby to get the list of rooms.
        PhotonNetwork.autoJoinLobby = true;

        // #Critical
        // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
        PhotonNetwork.automaticallySyncScene = true;
    }
	void Start () {
        Debug.Log("scene manager");
        if(isConnected){
            Debug.Log("already connected");
        }else{
            Debug.Log("Not connected yet, connecting now...");
            PhotonNetwork.ConnectUsingSettings(version);
        }

        //Custom properties for search
        expectedCustomPropertiesSchool = new ExitGames.Client.Photon.Hashtable { { "map", "school" } };
        expectedCustomPropertiesPlayground = new ExitGames.Client.Photon.Hashtable { { "map", "playground" } };

        //options for create new room
        roomOptions = new RoomOptions();
        roomOptions.IsVisible = true;
        roomOptions.MaxPlayers = 0;
        roomOptions.IsOpen = true;
	}
  
    /*
        Room Handling
     */
    public void ConnectToRoom(roomType type){
        Debug.Log("CONNECT TO ROOM, TYPE: " + type.ToString());
        map = type;
        if(isConnected){
            switch(map){
                case roomType.School:
                    Debug.Log("School");
                    PhotonNetwork.JoinRandomRoom(expectedCustomPropertiesSchool, 0);
                    break;
                case roomType.Playground:
                    Debug.Log("Playground");
                    PhotonNetwork.JoinRandomRoom(expectedCustomPropertiesPlayground, 0);
                break;
            }
        }else{
            Debug.Log("NOT CONNECTED TO MASTER SERVER");
        }
    }
    public void DisconectFromCurrentRoom(){
        Debug.Log("DISCONNECT FROM ROOM");
        PhotonNetwork.LeaveRoom();
    }

    #region Photon.PunBehaviour CallBacks

    public override void OnFailedToConnectToPhoton(DisconnectCause cause)
    {
        isConnected = false;
        Debug.Log("Failed to connect photon lobby, cause " + cause.ToString());
    }

    public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
        Debug.Log("Failed to join room, NOW WE CREATE ONE!!!");
        //if failed to connect to room, means that doesnt exist this room yet (or is full). So CREATE ONE!
        switch(map){
            case roomType.Playground:
                //create room        
                roomOptions.CustomRoomPropertiesForLobby = new string[1] { "map" };
                roomOptions.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable(1) { { "map", "playground" } };
                PhotonNetwork.CreateRoom(null, roomOptions, TypedLobby.Default);
                break;
            case roomType.School:
                //create room
                roomOptions.CustomRoomPropertiesForLobby = new string[1] { "map" };
                roomOptions.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable(1) { { "map", "school" } };
                PhotonNetwork.CreateRoom(null, roomOptions, TypedLobby.Default);
                break;
        }
    }
    public override void OnConnectedToMaster()
    {
        isConnected = true;
        Debug.Log("OnConnectedToMaster() was called by PUN");
    }
    public override void OnDisconnectedFromPhoton()
    {
        Debug.LogWarning("OnDisconnectedFromPhoton() was called by PUN");
    }
    public override void OnJoinedLobby()
    {
        isConnected = true;
        Debug.Log("____________ Joined photon lobby");
        Debug.Log("Will connect Room");
        Debug.Log("!!! room length : " + PhotonNetwork.GetRoomList().Length);
    }
    public override void OnJoinedRoom()
    {
        Debug.Log("Joined room! Number: " + PhotonNetwork.room.Name);
        Debug.Log("Room is open: " + PhotonNetwork.room.IsOpen);
        Debug.Log("Room custom prop: " + PhotonNetwork.room.CustomProperties["map"].ToString());
        if (PhotonNetwork.isMasterClient && PhotonNetwork.room.PlayerCount == 1)
        {
            Debug.Log("WE R MASTER CLIENT AND FIRST TO JOIN");
            switch (map)
            {
                case roomType.Playground:
                    PhotonNetwork.LoadLevel("Playground");
                    PhotonNetwork.isMessageQueueRunning = false;
                    break;
                case roomType.School:
                    PhotonNetwork.LoadLevel("School");
                    PhotonNetwork.isMessageQueueRunning = false;
                    break;
            }
        }else{
            Debug.Log("Not first to join");
        }

    }
    void OnLevelWasLoaded(){
        Debug.Log("______LEVEL WAS LOADED!!!");
        PhotonNetwork.isMessageQueueRunning = true;
        if (PhotonNetwork.isMessageQueueRunning)
        {
            PhotonNetwork.Instantiate("Character", spawn.transform.position, spawn.transform.rotation, 0);
        }
    }

    #endregion
}