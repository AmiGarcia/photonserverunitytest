﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkPlayer : Photon.MonoBehaviour {

    bool isAlive = true;
    Vector3 position;
    float lerpSmooting = 5f;

	void Start () {
        if(photonView.isMine){
            GetComponent<characterMovement>().enabled = true;
        }else{
            StartCoroutine("Alive");
        }
	}
	
    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){
        if(stream.isWriting){
            stream.SendNext(transform.position);
        }else{
            position = (Vector3)stream.ReceiveNext();
        }
    }

    //state machine:
    IEnumerator Alive(){
        while(isAlive){
            transform.position = Vector3.Lerp(transform.position, position, lerpSmooting * Time.deltaTime);

            yield return null;
        }
    }
}
