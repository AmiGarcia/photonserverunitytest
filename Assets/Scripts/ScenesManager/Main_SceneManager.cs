﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main_SceneManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
        UIManager.uiInstance.hudGameManager(false);
        UIManager.uiInstance.menuManager(true);
	}
	
}
