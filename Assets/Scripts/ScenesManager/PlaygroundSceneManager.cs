﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaygroundSceneManager : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        UIManager.uiInstance.hudGameManager(true);
        UIManager.uiInstance.menuManager(false);
    }
}
