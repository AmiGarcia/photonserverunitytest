﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class characterMovement : Photon.MonoBehaviour {

    public float width, speed;
	// Use this for initialization
	void Start () {
        speed = 0.3f;
        width = CameraScript.camWidth;
	}
	
	// Update is called once per frame
	void Update () {
        if(photonView.isMine){
            if (Input.GetMouseButton(0) && Input.mousePosition.x <= Screen.width / 2)
            {
                gameObject.transform.Translate(Vector3.left * speed);

            }
            if (Input.GetMouseButton(0) && Input.mousePosition.x > Screen.width / 2)
            {

                gameObject.transform.Translate(Vector3.right * speed);

            }
            if (Input.GetKey(KeyCode.RightArrow))
            {

                gameObject.transform.Translate(Vector3.right * speed);
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {

                gameObject.transform.Translate(Vector3.left * speed);

            }
        }
       
	}
}
